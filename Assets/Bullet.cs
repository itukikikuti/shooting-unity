﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bullet : MonoBehaviour
{
    void Start()
    {
        Destroy(this.gameObject, 10f);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.GetComponent<Player>().Dead();
        }

        if (collision.tag == "Enemy")
        {
            collision.gameObject.GetComponent<Enemy>().Dead();
            Destroy(this);
            this.GetComponent<ParticleSystem>().Stop();
        }
    }
}
