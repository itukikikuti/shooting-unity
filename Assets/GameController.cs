﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] GameObject enemy;
    float timer = 0f;

    void Update()
    {
        this.timer += Time.deltaTime;
        if (this.timer > 1f)
        {
            this.timer = 0f;
            GameObject clone = Instantiate(this.enemy);
            clone.transform.position = new Vector3(Random.Range(-3f, 3f), 6f, 0f);
        }
    }
}
