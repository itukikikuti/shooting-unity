﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    [SerializeField] GameObject dead;
    float timer = 0f;

    void Update()
    {
        this.transform.position += new Vector3(0f, -0.05f, 0f);

        this.timer += Time.deltaTime;
        if (this.timer > 1f)
        {
            this.timer = 0f;
            GameObject clone = Instantiate(this.bullet);
            clone.transform.position = this.transform.position + new Vector3(0f, -1f, 0f);

            GameObject player = GameObject.FindGameObjectWithTag("Player");

            clone.GetComponent<Rigidbody2D>().velocity = (player.transform.position - clone.transform.position).normalized * 5f;
        }
    }

    public void Dead()
    {
        Destroy(this.gameObject);

        GameObject clone = Instantiate(this.dead);
        clone.transform.position = this.transform.position;
        Destroy(clone, 2f);
    }
}
