﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] GameObject bullet;
    float timer = 0f;

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.position += new Vector3(0f, 0.1f, 0f);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.position += new Vector3(0f, -0.1f, 0f);
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.position += new Vector3(-0.1f, 0f, 0f);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.position += new Vector3(0.1f, 0f, 0f);
        }

        this.timer += Time.deltaTime;
        if (this.timer > 0.5f)
        {
            this.timer = 0f;
            GameObject clone = Instantiate(this.bullet);
            clone.transform.position = this.transform.position + new Vector3(0f, 1f, 0f);
            clone.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 5f);
        }
    }

    public void Dead()
    {
        SceneManager.LoadScene("Title");
    }
}
